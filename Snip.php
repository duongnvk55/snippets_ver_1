<?php

class Azebiz_Snip_Block_Snip extends Mage_Core_Block_Template
{
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
	
	public function isEnable()
	{
		return Mage::getStoreConfig('azebizsnippets/settings/enable');
	}
	
	public function isShowImage()
	{
		return Mage::getStoreConfig('azebizsnippets/settings/image');
	}
	public function isShowDescription()
	{
		return Mage::getStoreConfig('azebizsnippets/settings/description');
	}
	public function isRating()
	{
		return Mage::getStoreConfig('azebizsnippets/settings/rating');
	}
	public function isPrice()
	{
		return Mage::getStoreConfig('azebizsnippets/settings/price');
	}
	public function isAvailability()
	{
		return Mage::getStoreConfig('azebizsnippets/settings/availability');
	}
	
	public function getProduct()
	{
		return Mage::registry('product');
	}
	
}