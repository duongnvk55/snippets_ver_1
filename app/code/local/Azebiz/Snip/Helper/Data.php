<?php

class Azebiz_Snip_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getBreadCrumbs()
	{
		return Mage::getStoreConfig('snip/general/breadcrumbs');
	}
	public function getCategory()
	{
		return Mage::getStoreConfig('snip/general/category');
	}
}