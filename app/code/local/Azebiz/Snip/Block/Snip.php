<?php

class Azebiz_Snip_Block_Snip extends Mage_Core_Block_Template
{
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
	
	public function isEnable()
	{
		return Mage::getStoreConfig('snip/general/enable');
	}
	
	public function isShowImage()
	{
		return Mage::getStoreConfig('snip/general/image');
	}
	public function isShowDescription()
	{
		return Mage::getStoreConfig('snip/general/description');
	}
	public function isRating()
	{
		return Mage::getStoreConfig('snip/general/rating');
	}
	public function isPrice()
	{
		return Mage::getStoreConfig('snip/general/price');
	}
	public function isAvailability()
	{
		return Mage::getStoreConfig('snip/general/availability');
	}
	
	public function getProduct()
	{
		return Mage::registry('product');
	}
	
	 public function getBreadCrumbs()
	{
		return Mage::getStoreConfig('snip/general/breadcrumbs');
	}
	public function getCategory()
	{
		return Mage::getStoreConfig('snip/general/category');
	}
	
}